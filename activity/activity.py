# 1 problem solution
year = int(input("Please enter year here: \n"))


if (year % 4 == 0) and (year % 100 != 0):
    print(f"{year} is a leap year.")
elif (year % 400 == 0) and (year % 100 == 0):
    print(f"{year} is a leap year.")
elif (year <= 0):
    print(f"{year} is not a valid year format")
else:
    print(f"{year} is not a leap year.")

# 2 problem solution
rows = int(input("Enter number of rows: \n"))
columns = int(input("Enter number of columns: \n"))

for i in range (rows):
    print('* ' * columns)

# Instructor's alternative solution
i = 1
j = 1
final_output = ""
while i <= rows:
    while j <= columns:
        final_output += "*"
        j += 1        
    print(final_output)
    i += 1