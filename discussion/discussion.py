# [SECTION] Taking input from user
username = input("Please enter your name:\n")
print(f"Hello {username}! Welcome to the Python Short Course!")

num1 = input("Enter first number:\n")
num2 = input("Enter second number:\n")

# If you try to concatenate the value of inputs, it will just combine them since they are both strings. In order for us to get the right numeric result of the addition of 2 numbers, we have to put the two numbers inside the 'int()' function to convert them into a numbers first before the addition process.
print(f"The sum of num1 and num2 is {int(num1) + int(num2)}")


# Type casting
# int()
# str()
# float()

# [SECTION] If-else statements
test_num = 75

if test_num >= 60:
    print("Test passed")
else:
    print("Test failed")

test_num2 = int(input("Please enter the second test number. \n"))

# If else statements in python don't have curly brackets but instead rely on the spacing of the indention to denote the scope of the statement. It also uses the colon ':' to let the compiler know that the statement continues.
if test_num2 > 0:
    print("The number is positive")
elif test_num2 == 0:
    print("The number is zero")
else:
    print("The number is negative")

# [SECTION] Loops
# While Loop
i = 1 # initial variable
while i <= 5: # condition
    print(f"Current count {i}") # expression to be executed
    i += 1 # incrementor

# For Loop
fruits = ["apple", "banana", "cherry"]
for fruit in fruits:
    print(fruit)

# range() function
# gives a range of numbers for us to loop to and you can pass up to 3 arguments into it.
# 1st argument: where the range starts
# 2nd argument: where the range ends
# 3rd argument: incrementing of the range
for x in range(6):
    print(f"The current value is {x}")

for x in range(5, 10):
    print(f"The current value is {x}")

for x in range(5, 20, 2):
    print(f"The current value is {x}")

# [SECTION] Break Statement
# Break statement terminates/stops the loop
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j += 1

# Continue statement immediately returns the loop back to the condition part. It ignores any other code that is written after it.
k = 1
while k < 6:
    k += 1
    if k == 3:
        continue
    print(k)